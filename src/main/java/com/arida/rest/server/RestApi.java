package com.arida.rest.server;

import com.arida.rest.model.ExampleXml;
import com.arida.rest.model.Parameter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RestApi {

    @RequestMapping("/example")
    public ExampleXml getExampleXml() {
        ExampleXml exampleXml = new ExampleXml();
        List<Parameter> params = new ArrayList<>();
        Parameter parameter1 = new Parameter("parameter1", "STRING", "Max Mustermann");
        Parameter parameter2 = new Parameter("parameter2", "REAL", "1234");
        params.add(parameter1);
        params.add(parameter2);
        exampleXml.setParameters(params);
        return exampleXml;
    }
}
