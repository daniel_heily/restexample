<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <NewDataSet>
            <xsl:apply-templates/>
        </NewDataSet>
    </xsl:template>
    <xsl:template match="parameters">
        <xsl:for-each select="parameter">
            <PARAMETER>
                <xsl:variable name="name"><xsl:value-of select="@name"/></xsl:variable>
                <NAME><xsl:value-of select="$name"/></NAME>
                <TYPE><xsl:value-of select="@type"/></TYPE>
                <VALUE><xsl:value-of select="current()"/></VALUE>
            </PARAMETER>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>